﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    public class Animal : ICloneable
    {
        internal Detalles Rasgos; // Operacion 

        public int Patas { get; set; } //intancias
        public string Nombre { get; set; } //intancias

        public Detalles rasgos { get; set; } //intancias
        public object Clone() //metodo
        {
            Animal clonado = this.MemberwiseClone() as Animal; //Asignar Operaciones
            Detalles detalles = new Detalles();
            detalles.Color = this.Rasgos.Color;
            detalles.Raza = this.Rasgos.Raza;
            clonado.Rasgos = detalles;
            return clonado;

            // return this.MemberwiseClone(); //Shallow 
        }
    }
    public class Detalles
    {
        public string Color { get; set; }
        public string Raza { get; set; }
    }
}
