﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Prototype
{
    class Program
    {
        static void Main(string[] args)
        {
            Animal oAnimal = new Animal() { Nombre= "Oveja Dolly", Patas =4 }; //atributos
            oAnimal.Rasgos = new Detalles();
            oAnimal.Rasgos.Color = "Blanca";
            oAnimal.Rasgos.Raza = "Oveja";


            Animal oAnimalClonado = oAnimal.Clone() as Animal;

            oAnimalClonado.Patas =5;

            oAnimalClonado.Rasgos.Color = "Negro";
            oAnimalClonado.Nombre = "Oveja Negra";

            Console.WriteLine(oAnimal.Patas);
            //Console.WriteLine(oAnimalClonado.Rasgos.Color);

            Console.WriteLine("Color animal original: " + oAnimal.Rasgos.Color);
            Console.WriteLine("Color animal clonado: " + oAnimalClonado.Rasgos.Color);
            Console.WriteLine("Nombre animal original: " + oAnimal.Nombre);
            Console.WriteLine("Nombre animal clonado: " + oAnimalClonado.Nombre);
        }
    }
}
